# Honeygain Earning Monitor

### Steps to run
1. Rename the directory `config_example` to `config`.
2. Edit the file as needed.
3. `docker-compose up -d --build`

That's all