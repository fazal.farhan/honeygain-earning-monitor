import io
import json
import os
import threading
from time import sleep
from pyHoneygain import HoneyGain
from datetime import datetime,timezone
from discord_webhook import DiscordEmbed, DiscordWebhook


def read_file():
    with io.open('config/credentials.json', 'r', encoding='utf-8') as stream:
        return json.load(stream)


def update_balance(email):
    try:
        api = HoneyGain()
        api.set_proxy(credentials.get(email).get('proxy'))
        api.set_jwt_token(jwt_tokens.get(email))
        total[email] = api.stats_today_jt().get('total').get('credits')
        active_devices_count[email] = api.me().get('active_devices_count')
        print(f"Got: {email}")
    except Exception as e:
        print(f"Error {e}")
        errors.append(email)

def send_notification(errors, total, active_devices_count):
    print('[☑️] Sending notification..')
    webhook = DiscordWebhook(url=os.environ.get('WEBHOOK_URL'), rate_limit_retry=True)
    total_credits = sum(total.values())

    amount_earned = total_credits/1000
    try:
        hourly_average = (total_credits/1000)/datetime.now(timezone.utc).hour
    except ZeroDivisionError:
        hourly_average = 0
    try:
        device_average = (total_credits/10)/sum(active_devices_count.values())
    except ZeroDivisionError:
        device_average = ':unamused:'

    webhook.content = f'''{f"A total of `{len(errors)}` errors in total." if len(errors) > 0 else "No Errors ✅"}
`{'Credits':20s}{total_credits:10.2f}` :honey_pot:
`{'Amount':20s}{amount_earned:10.2f}` :dollar:
`{'Active Devices':20s}{sum(active_devices_count.values()):10d}` :man_technologist:
`{"Today's Average":20s}{hourly_average:6.2f}$/hr` :hourglass:
'''
# `{"Device's Average":20s}{device_average:6.2f}¢/device` :robot:
    webhook.execute()
    print('[☑️] Notification Sent..!')


jwt_tokens = read_file()
credentials = {}
proxies =  [line.strip() for line in io.open('config/proxy.conf', 'r', encoding='utf-8').readlines()]

for email,proxy in zip(list(jwt_tokens.keys()), proxies):
    credentials[email] = {
        'proxy':proxy
    }


first_run = True

while (True):
    if (datetime.now(timezone.utc).strftime("%M") == str(f"{os.environ.get('DELAY', 0):02}")) or first_run:
        first_run = False
        threads = []

        total = {}
        errors = []
        active_devices_count = {}
        for email in jwt_tokens.keys():
            threads.append(threading.Thread(target=update_balance, args=[email]))

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

        print(f"A total of {len(errors)} errors in total.")

        # for email, balance in sorted(total.items()):
        #     print(f'{email:25s}: {balance}')

        for email in errors:
            print(email)

        total_credits = sum(total.values())
        print(f'Total Credits: {total_credits:10.2f} HG')
        print(f'Total Amount : {total_credits/1000:10.2f} $')
        send_notification(errors, total, active_devices_count)
        sleep(120)