FROM python:alpine
ENV PYTHONUNBUFFERED=true
RUN pip install discord_webhook requests pysocks
WORKDIR /app
CMD [ "python", "-u", "balance_updater.py" ]
COPY app /app
